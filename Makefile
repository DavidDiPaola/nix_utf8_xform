# licensed under CC0 (https://creativecommons.org/public-domain/cc0/)
# 2023 David DiPaola

SRC = main.c table_aesthetic.c table_romanize.c
BIN = utf8_xform

OBJ = $(SRC:.c=.o)

_CFLAGS = \
	-std=c99 \
	-Wall -Wextra \
	-Os \
	-g \
	$(CFLAGS)

_LDFLAGS = \
	-O1 \
	$(LDFLAGS)

.PHONY: all
all: $(BIN)

.PHONY: clean
clean:
	rm -f $(OBJ)
	rm -f $(BIN)

%.o: %.c
	$(CC) $(_CFLAGS) -c $< -o $@

$(BIN): $(OBJ)
	$(CC) $(_LDFLAGS) -o $@ $^
