# nix/utf8_xform
a 'nix tool that transforms UTF-8 text

## features
- convert boring text into ａｅｓｔｈｅｔｉｃ text:
```
$ echo 'i have farted' | utf8_xform aesthetic
ｉ　ｈａｖｅ　ｆａｒｔｅｄ
```
- decode text into unicode codepoints:
```
$ echo 'hello world!' | utf8_xform aesthetic | utf8_xform decode
U+FF48 ｈ
U+FF45 ｅ
U+FF4C ｌ
U+FF4C ｌ
U+FF4F ｏ
U+3000 ideographic space
U+FF57 ｗ
U+FF4F ｏ
U+FF52 ｒ
U+FF4C ｌ
U+FF44 ｄ
U+FF01 ！
U+000A line feed (LF, \n)
```
- convert some non-latin texts into latin text (romanization):
```
$ echo 'カタカナ' | utf8_xform romanize
KaTaKaNa
```
- randomize case:
```
$ echo 'this is what you sound like' | utf8_xform randomcase
THis iS WhaT you sounD likE
```

## building
this project should build in any linux environment and probably on any 'nix system.
just run `make`.
