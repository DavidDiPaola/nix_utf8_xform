/*
licensed under CC0 (https://creativecommons.org/public-domain/cc0/)
2023 David DiPaola
*/

#ifndef __TABLE_ROMANIZE_H
#define __TABLE_ROMANIZE_H

#include "pair.h"

extern const struct pair table_ROMANIZE[];

extern const int table_ROMANIZE_LENGTH;

#endif
