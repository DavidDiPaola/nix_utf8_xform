#!/bin/sh

set -eu

BIN="$(dirname $0)/../utf8_xform"
STATUS=0

runTest () {
	NAME="$1"
	INPUT="$2"
	EXPECTED="$3"

	OUTPUT="$(printf '%s' "$INPUT" | ./"$BIN" aesthetic)"

	printf '%s ' "$NAME"
	if [ "$OUTPUT" = "$EXPECTED" ] ; then
		printf '[PASS]\n'
	else
		printf '[FAIL]\n'
		printf 'expected: "%s"\n' "$EXPECTED"
		printf 'output: "%s"\n' "$OUTPUT"
		STATUS=1
	fi
}

# symbols 0x20-0x2F,0x3A-0x40,0x5B-0x60,0x7B-0x7E
runTest \
	'ASCII symbols' \
	" !\"#\$%&'()*+,-./:;<=>?@[\\]^_\`{|}~" \
	'　！＂＃＄％＆＇（）＊＋，－．／：；＜＝＞？＠［＼］＾＿｀｛｜｝〜' \
;

runTest \
	'ASCII numbers' \
	'0123456789' \
	'０１２３４５６７８９' \
;

runTest \
	'ASCII uppercase' \
	'ABCDEFGHIJKLMNOPQRSTUVWXYZ' \
	'ＡＢＣＤＥＦＧＨＩＪＫＬＭＮＯＰＱＲＳＴＵＶＷＸＹＺ' \
;

runTest \
	'ASCII lowercase' \
	'abcdefghijklmnopqrstuvwxyz' \
	'ａｂｃｄｅｆｇｈｉｊｋｌｍｎｏｐｑｒｓｔｕｖｗｘｙｚ' \
;

if [ $STATUS -eq 0 ] ; then
	printf '\nall tests passed\n'
fi
exit $STATUS
