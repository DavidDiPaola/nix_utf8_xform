/*
licensed under CC0 (https://creativecommons.org/public-domain/cc0/)
2023 David DiPaola
*/

#include "pair.h"

const struct pair table_ROMANIZE[] = {

	/*
	TODO
		arabic? (rtl issues?)
		aramaic? (rtl issues?)
		armenian
		georgian
		greek (see https://en.wikipedia.org/wiki/Romanization_of_Greek)
		hebrew? (rtl issues?)
		indian scripts (see https://en.wikipedia.org/wiki/Official_scripts_of_the_Republic_of_India)
		kazakh?
		khmer
		korean (hangul)
		mongolian?
		myanma
		nepali?
		pashtu?
		sanskrit?
		tamil?
		thai
		tibetan
		ukranian?
		urdu?
	*/

	/* japanese - katakana (see https://en.wikipedia.org/wiki/Katakana_(Unicode_block)) */
	{ "ァ", "A"   },
	{ "ア", "A"   },
	{ "ィ", "I"   },
	{ "イ", "I"   },
	{ "ゥ", "U"   },
	{ "ウ", "U"   },
	{ "ェ", "E"   },
	{ "エ", "E"   },
	{ "ォ", "O"   },
	{ "オ", "O"   },
	{ "ヵ", "Ka"  },
	{ "カ", "Ka"  },
	{ "ガ", "Ga"  },
	{ "キ", "Ki"  },
	{ "ギ", "Gi"  },
	{ "ク", "Ku"  },
	{ "グ", "Gu"  },
	{ "ヶ", "Ke"  },
	{ "ケ", "Ke"  },
	{ "ゲ", "Ge"  },
	{ "コ", "Ko"  },
	{ "ゴ", "Go"  },
	{ "サ", "Sa"  },
	{ "ザ", "Za"  },
	{ "シ", "Shi" },
	{ "ジ", "Zi"  },
	{ "ス", "Su"  },
	{ "ズ", "Zu"  },
	{ "セ", "Se"  },
	{ "ゼ", "Ze"  },
	{ "ソ", "So"  },
	{ "ゾ", "Zo"  },
	{ "タ", "Ta"  },
	{ "ダ", "Da"  },
	{ "チ", "Ti"  },
	{ "ヂ", "Di"  },
	{ "ッ", "Tu"  },
	{ "ツ", "Tu"  },
	{ "ヅ", "Du"  },
	{ "テ", "Te"  },
	{ "デ", "De"  },
	{ "ト", "To"  },
	{ "ド", "Do"  },
	{ "ナ", "Na"  },
	{ "ニ", "Ni"  },
	{ "ヌ", "Nu"  },
	{ "ネ", "Ne"  },
	{ "ノ", "No"  },
	{ "ハ", "Ha"  },
	{ "バ", "Ba"  },
	{ "パ", "Pa"  },
	{ "ヒ", "Hi"  },
	{ "ビ", "Bi"  },
	{ "ピ", "Pi"  },
	{ "フ", "Hu"  },
	{ "ブ", "Bu"  },
	{ "プ", "Pu"  },
	{ "ヘ", "He"  },
	{ "ベ", "Be"  },
	{ "ペ", "Pe"  },
	{ "ホ", "Ho"  },
	{ "ボ", "Bo"  },
	{ "ポ", "Po"  },
	{ "マ", "Ma"  },
	{ "ミ", "Mi"  },
	{ "ム", "Mu"  },
	{ "メ", "Me"  },
	{ "モ", "Mo"  },
	{ "ャ", "Ya"  },
	{ "ヤ", "Ya"  },
	{ "ュ", "Yu"  },
	{ "ユ", "Yu"  },
	{ "ョ", "Yo"  },
	{ "ヨ", "Yo"  },
	{ "ラ", "Ra"  },
	{ "リ", "Ri"  },
	{ "ル", "Ru"  },
	{ "レ", "Re"  },
	{ "ロ", "Ro"  },
	{ "ヮ", "Wa"  },
	{ "ワ", "Wa"  },
	{ "ヰ", "Wi"  },
	{ "ヱ", "We"  },
	{ "ヲ", "Wo"  },
	{ "ン", "N"   },
	{ "ヴ", "Vu"  },

	/* japanese - hiragana (see https://en.wikipedia.org/wiki/Hiragana_(Unicode_block)) */
	{ "ぁ", "A"   },
	{ "あ", "A"   },
	{ "ぃ", "I"   },
	{ "い", "I"   },
	{ "ぅ", "U"   },
	{ "う", "U"   },
	{ "ぇ", "E"   },
	{ "え", "E"   },
	{ "ぉ", "O"   },
	{ "お", "O"   },
	{ "か", "Ka"  },
	{ "が", "Ga"  },
	{ "き", "Ki"  },
	{ "ぎ", "Gi"  },
	{ "く", "Ku"  },
	{ "ぐ", "Gu"  },
	{ "け", "Ke"  },
	{ "げ", "Ge"  },
	{ "こ", "Ko"  },
	{ "ご", "Go"  },
	{ "さ", "Sa"  },
	{ "ざ", "Za"  },
	{ "し", "Si"  },
	{ "じ", "Zi"  },
	{ "す", "Su"  },
	{ "ず", "Zu"  },
	{ "せ", "Se"  },
	{ "ぜ", "Ze"  },
	{ "そ", "So"  },
	{ "ぞ", "Zo"  },
	{ "た", "Ta"  },
	{ "だ", "Da"  },
	{ "ち", "Chi" },  /* aka Ti */
	{ "ぢ", "Di"  },
	{ "っ", "Tsu" },  /* aka Tu */
	{ "つ", "Tsu" },  /* aka Tu */
	{ "づ", "Du"  },
	{ "て", "Te"  },
	{ "で", "De"  },
	{ "と", "To"  },
	{ "ど", "Do"  },
	{ "な", "Na"  },
	{ "に", "Ni"  },
	{ "ぬ", "Nu"  },
	{ "ね", "Ne"  },
	{ "の", "No"  },
	{ "は", "Ha"  },
	{ "ば", "Ba"  },
	{ "ぱ", "Pa"  },
	{ "ひ", "Hi"  },
	{ "び", "Bi"  },
	{ "ぴ", "Pi"  },
	{ "ふ", "Hu"  },
	{ "ぶ", "Bu"  },
	{ "ぷ", "Pu"  },
	{ "ぷ", "Pu"  },
	{ "へ", "He"  },
	{ "べ", "Be"  },
	{ "ぺ", "Pe"  },
	{ "ほ", "Ho"  },
	{ "ぼ", "Bo"  },
	{ "ぽ", "Po"  },
	{ "ま", "Ma"  },
	{ "み", "Mi"  },
	{ "む", "Mu"  },
	{ "め", "Me"  },
	{ "も", "Mo"  },
	{ "ゃ", "Ya"  },
	{ "や", "Ya"  },
	{ "ゅ", "Yu"  },
	{ "ゆ", "Yu"  },
	{ "ょ", "Yo"  },
	{ "よ", "Yo"  },
	{ "ら", "Ra"  },
	{ "り", "Ri"  },
	{ "る", "Ru"  },
	{ "れ", "Re"  },
	{ "ろ", "Ro"  },
	{ "ゎ", "Wa"  },
	{ "わ", "Wa"  },
	{ "ゐ", "Wi"  },
	{ "ゑ", "We"  },
	{ "を", "Wo"  },
	{ "ん", "N"   },
	{ "ゔ", "Vu"  },

	/* japanese - symbols (see https://en.wikipedia.org/wiki/Japanese_punctuation) */
	{ "‥",  ".."  },  /* U+2025 */
	{ "…",  "..." },  /* U+2026 */
	{ "、", ","   },  /* U+3001 */
	{ "。", "."   },  /* U+3002 */
	{ "「", "'"   },  /* U+300C */
	{ "」", "'"   },  /* U+300D */
	{ "『", "\""  },  /* U+300E */
	{ "』", "\""  },  /* U+300F */
	{ "〜", "~"   },  /* U+301C */
	{ "〝", "\""  },  /* U+301D */
	{ "〟", "\""  },  /* U+301F */
	{ "・", " "   },  /* U+30FB interpunct */
	{ "！", "!"   },  /* U+FF01 */
	{ "，", ","   },  /* U+FF0C */
	{ "：", ":"   },  /* U+FF1A */
	{ "？", "?"   },  /* U+FF1F */
	{ "･",  " "   },  /* U+FF65 half width interpunct */

	/* russian - modern letters (see https://en.wikipedia.org/wiki/Romanization_of_Russian) */
	{ "А", "A"   },
	{ "а", "a"   },
	{ "Б", "B"   },
	{ "б", "b"   },
	{ "В", "V"   },
	{ "в", "v"   },
	{ "Г", "G"   },
	{ "г", "g"   },
	{ "Д", "D"   },
	{ "д", "d"   },
	{ "Е", "E"   },
	{ "е", "e"   },
	{ "Ё", "YE"  },
	{ "ё", "ye"  },
	{ "Ж", "ZH"  },
	{ "ж", "zh"  },
	{ "З", "Z"   },
	{ "з", "Z"   },
	{ "И", "I"   },
	{ "и", "i"   },
	{ "Й", "J"   },
	{ "й", "j"   },
	{ "К", "K"   },
	{ "к", "k"   },
	{ "Л", "L"   },
	{ "л", "l"   },
	{ "М", "M"   },
	{ "м", "m"   },
	{ "Н", "N"   },
	{ "н", "n"   },
	{ "О", "O"   },
	{ "о", "o"   },
	{ "П", "P"   },
	{ "п", "p"   },
	{ "Р", "R"   },
	{ "р", "r"   },
	{ "С", "S"   },
	{ "с", "s"   },
	{ "Т", "T"   },
	{ "т", "t"   },
	{ "У", "U"   },
	{ "у", "u"   },
	{ "Ф", "F"   },
	{ "ф", "f"   },
	{ "Х", "KH"  },
	{ "х", "kh"  },
	{ "Ц", "C"   },
	{ "ц", "c"   },
	{ "Ч", "CH"  },
	{ "ч", "ch"  },
	{ "Ш", "SH"  },
	{ "ш", "sh"  },
	{ "Щ", "SHH" },
	{ "щ", "shh" },
	{ "Ы", "Y"   },
	{ "ы", "y"   },
	{ "Ь", "'"   },  /* TODO soft sign */
	{ "ь", "'"   },  /* TODO soft sign */
	{ "Э", "EH"  },
	{ "э", "eh"  },
	{ "Ю", "JU"  },
	{ "ю", "ju"  },
	{ "Я", "JA"  },
	{ "я", "ja"  },

	/* russian - pre-1918 letters (see https://en.wikipedia.org/wiki/Romanization_of_Russian) */
	{ "І", "I"  },
	{ "і", "i"  },
	{ "Ѳ", "FH" },
	{ "ѳ", "fh" },
	{ "Ѣ", "YE" },
	{ "ѣ", "ye" },
	{ "Ѵ", "YH" },
	{ "ѵ", "yh" },

};

const int table_ROMANIZE_LENGTH = (sizeof(table_ROMANIZE) / sizeof(*table_ROMANIZE));
