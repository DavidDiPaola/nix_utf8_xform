/*
licensed under CC0 (https://creativecommons.org/public-domain/cc0/)
2023 David DiPaola
*/

#include "pair.h"

const struct pair table_AESTHETIC[] = {

	/*
	ASCII characters to fullwidth/ideographic equivalents
	U+30xx characters: see https://en.wikipedia.org/wiki/CJK_Symbols_and_Punctuation
	U+FFxx characters: see https://en.wikipedia.org/wiki/Halfwidth_and_Fullwidth_Forms_(Unicode_block)
	*/
	{ " ",  "　" },  /* U+20 --> U+3000 */
	{ "!",  "！" },  /* U+21 --> U+FF01 */
	{ "\"", "＂" },  /* U+22 --> U+FF02 */
	{ "#",  "＃" },  /* U+23 --> U+FF03 */
	{ "$",  "＄" },  /* U+24 --> U+FF04 */
	{ "%",  "％" },  /* U+25 --> U+FF05 */
	{ "&",  "＆" },  /* U+26 --> U+FF06 */
	{ "'",  "＇" },  /* U+27 --> U+FF07 */
	{ "(",  "（" },  /* U+28 --> U+FF08 */
	{ ")",  "）" },  /* U+29 --> U+FF09 */
	{ "*",  "＊" },  /* U+2A --> U+FF0A */
	{ "+",  "＋" },  /* U+2B --> U+FF0B */
	{ ",",  "，" },  /* U+2C --> U+FF0C */
	{ "-",  "－" },  /* U+2D --> U+FF0D */
	{ ".",  "．" },  /* U+2E --> U+FF0E */
	{ "/",  "／" },  /* U+2F --> U+FF0F */
	{ "0",  "０" },  /* U+30 --> U+FF10 */
	{ "1",  "１" },  /* U+31 --> U+FF11 */
	{ "2",  "２" },  /* U+32 --> U+FF12 */
	{ "3",  "３" },  /* U+33 --> U+FF13 */
	{ "4",  "４" },  /* U+34 --> U+FF14 */
	{ "5",  "５" },  /* U+35 --> U+FF15 */
	{ "6",  "６" },  /* U+36 --> U+FF16 */
	{ "7",  "７" },  /* U+37 --> U+FF17 */
	{ "8",  "８" },  /* U+38 --> U+FF18 */
	{ "9",  "９" },  /* U+39 --> U+FF19 */
	{ ":",  "：" },  /* U+3A --> U+FF1A */
	{ ";",  "；" },  /* U+3B --> U+FF1B */
	{ "<",  "＜" },  /* U+3C --> U+FF1C */
	{ "=",  "＝" },  /* U+3D --> U+FF1D */
	{ ">",  "＞" },  /* U+3E --> U+FF1E */
	{ "?",  "？" },  /* U+3F --> U+FF1F */
	{ "@",  "＠" },  /* U+40 --> U+FF20 */
	{ "A",  "Ａ" },  /* U+41 --> U+FF21 */
	{ "B",  "Ｂ" },  /* U+42 --> U+FF22 */
	{ "C",  "Ｃ" },  /* U+43 --> U+FF23 */
	{ "D",  "Ｄ" },  /* U+44 --> U+FF24 */
	{ "E",  "Ｅ" },  /* U+45 --> U+FF25 */
	{ "F",  "Ｆ" },  /* U+46 --> U+FF26 */
	{ "G",  "Ｇ" },  /* U+47 --> U+FF27 */
	{ "H",  "Ｈ" },  /* U+48 --> U+FF28 */
	{ "I",  "Ｉ" },  /* U+49 --> U+FF29 */
	{ "J",  "Ｊ" },  /* U+4A --> U+FF2A */
	{ "K",  "Ｋ" },  /* U+4B --> U+FF2B */
	{ "L",  "Ｌ" },  /* U+4C --> U+FF2C */
	{ "M",  "Ｍ" },  /* U+4D --> U+FF2D */
	{ "N",  "Ｎ" },  /* U+4E --> U+FF2E */
	{ "O",  "Ｏ" },  /* U+4F --> U+FF2F */
	{ "P",  "Ｐ" },  /* U+50 --> U+FF30 */
	{ "Q",  "Ｑ" },  /* U+51 --> U+FF31 */
	{ "R",  "Ｒ" },  /* U+52 --> U+FF32 */
	{ "S",  "Ｓ" },  /* U+53 --> U+FF33 */
	{ "T",  "Ｔ" },  /* U+54 --> U+FF34 */
	{ "U",  "Ｕ" },  /* U+55 --> U+FF35 */
	{ "V",  "Ｖ" },  /* U+56 --> U+FF36 */
	{ "W",  "Ｗ" },  /* U+57 --> U+FF37 */
	{ "X",  "Ｘ" },  /* U+58 --> U+FF38 */
	{ "Y",  "Ｙ" },  /* U+59 --> U+FF39 */
	{ "Z",  "Ｚ" },  /* U+5A --> U+FF3A */
	{ "[",  "［" },  /* U+5B --> U+FF3B */
	{ "\\", "＼" },  /* U+5C --> U+FF3C */
	{ "]",  "］" },  /* U+5D --> U+FF3D */
	{ "^",  "＾" },  /* U+5E --> U+FF3E */
	{ "_",  "＿" },  /* U+5F --> U+FF3F */
	{ "`",  "｀" },  /* U+60 --> U+FF40 */
	{ "a",  "ａ" },  /* U+61 --> U+FF41 */
	{ "b",  "ｂ" },  /* U+62 --> U+FF42 */
	{ "c",  "ｃ" },  /* U+63 --> U+FF43 */
	{ "d",  "ｄ" },  /* U+64 --> U+FF44 */
	{ "e",  "ｅ" },  /* U+65 --> U+FF45 */
	{ "f",  "ｆ" },  /* U+66 --> U+FF46 */
	{ "g",  "ｇ" },  /* U+67 --> U+FF47 */
	{ "h",  "ｈ" },  /* U+68 --> U+FF48 */
	{ "i",  "ｉ" },  /* U+69 --> U+FF49 */
	{ "j",  "ｊ" },  /* U+6A --> U+FF4A */
	{ "k",  "ｋ" },  /* U+6B --> U+FF4B */
	{ "l",  "ｌ" },  /* U+6C --> U+FF4C */
	{ "m",  "ｍ" },  /* U+6D --> U+FF4D */
	{ "n",  "ｎ" },  /* U+6E --> U+FF4E */
	{ "o",  "ｏ" },  /* U+6F --> U+FF4F */
	{ "p",  "ｐ" },  /* U+70 --> U+FF50 */
	{ "q",  "ｑ" },  /* U+71 --> U+FF51 */
	{ "r",  "ｒ" },  /* U+72 --> U+FF52 */
	{ "s",  "ｓ" },  /* U+73 --> U+FF53 */
	{ "t",  "ｔ" },  /* U+74 --> U+FF54 */
	{ "u",  "ｕ" },  /* U+75 --> U+FF55 */
	{ "v",  "ｖ" },  /* U+76 --> U+FF56 */
	{ "w",  "ｗ" },  /* U+77 --> U+FF57 */
	{ "x",  "ｘ" },  /* U+78 --> U+FF58 */
	{ "y",  "ｙ" },  /* U+79 --> U+FF59 */
	{ "z",  "ｚ" },  /* U+7A --> U+FF5A */
	{ "{",  "｛" },  /* U+7B --> U+FF5B */
	{ "|",  "｜" },  /* U+7C --> U+FF5C */
	{ "}",  "｝" },  /* U+7D --> U+FF5D */
	{ "~",  "〜" },  /* U+7E --> U+301C */

};

const int table_AESTHETIC_LENGTH = (sizeof(table_AESTHETIC) / sizeof(*table_AESTHETIC));
