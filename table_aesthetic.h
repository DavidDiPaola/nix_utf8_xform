/*
licensed under CC0 (https://creativecommons.org/public-domain/cc0/)
2023 David DiPaola
*/

#ifndef __TABLE_AESTHETIC_H
#define __TABLE_AESTHETIC_H

#include "pair.h"

extern const struct pair table_AESTHETIC[];

extern const int table_AESTHETIC_LENGTH;

#endif
