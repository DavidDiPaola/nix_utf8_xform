/*
licensed under CC0 (https://creativecommons.org/public-domain/cc0/)
2023 David DiPaola
*/

#include <unistd.h>

#include <stdlib.h>

#include <string.h>

#include <stdio.h>

#include <errno.h>

#include <stdint.h>

#include <time.h>

#include "pair.h"

#include "table_aesthetic.h"

#include "table_romanize.h"

/*******************************************************************************
utility functions
*******************************************************************************/

static void
_readStdInOrExit(char * buf, size_t buf_size) {
	ssize_t amount = read(/*fd=*/0, buf, buf_size);
	if (amount == 0) {
		exit(0);
	}
	else if (amount < 0) {
		perror("ERROR: _readStdInOrExit()");
		exit(1);
	}
}

static size_t
_utf8CharSizeOrExit(const char * buf) {
	uint8_t ch = (unsigned)(buf[0]);
	if (ch < 0x7F) {
		return 1;
	}
	else if ( (ch & (0b111<<5)) == (0b110<<5) ) {
		return 2;
	}
	else if ( (ch & (0b1111<<4)) == (0b1110<<4) ) {
		return 3;
	}
	else if ( (ch & (0b11111<<3)) == (0b11110<<3) ) {
		return 4;
	}

	fprintf(stderr, "ERROR: _utf8CharSize(): could not determine UTF-8 char size" "\n");
	exit(1);
}

/*
NOTE size of buf must be >= 5
*/
static size_t
_readUtf8CharOrExit(char * buf, size_t buf_size) {
	memset(buf, '\0', sizeof(buf_size));

	_readStdInOrExit(buf, 1);
	size_t charSize = _utf8CharSizeOrExit(buf);

	if (charSize == 2) {
		_readStdInOrExit(buf + 1, 1);
	}
	else if (charSize == 3) {
		_readStdInOrExit(buf + 1, 2);
	}
	else if (charSize == 4) {
		_readStdInOrExit(buf + 1, 3);
	}

	return charSize;
}

struct _codepointPair {
	uint32_t codepoint;
	const char * description;
};

static void
_decode_printOrExit(const uint8_t * buf, uint32_t codepoint) {
	/* print U+xxxx part */

	if (codepoint <= 0xFFFF) {
		printf("U+%04X", codepoint);
	}
	else if (codepoint <= 0x10FFFF) {
		printf("U+%06X", codepoint);
	}
	else {
		fprintf(stderr, "ERROR: _decode(): codepoint out of range (0x%08X)" "\n", codepoint);
		exit(1);
	}

	/* print character part */

	const char * disp = NULL;
	const struct _codepointPair INVISIBLE[] = {
		/* TODO add escape sequences and keyboard keys */
		/* ASCII control characters (see https://en.wikipedia.org/wiki/List_of_Unicode_characters#Control_codes) */
		/* escape sequences from https://en.wikipedia.org/wiki/C0_and_C1_control_codes */
		{ 0x00, "null character (NUL, \\0)" },
		{ 0x01, "start of heading (SOH)" },
		{ 0x02, "start of text (STX)" },
		{ 0x03, "end of text (ETX)" },
		{ 0x04, "end of transmission (EOT)" },
		{ 0x05, "enquiry (ENQ)" },
		{ 0x06, "acknowledge (ACK)" },
		{ 0x07, "bell (BEL, \\a)" },
		{ 0x08, "backspace (BS, \\b)" },
		{ 0x09, "horizontal tab (HT, \\t)" },
		{ 0x0A, "line feed (LF, \\n)" },
		{ 0x0B, "vertical tab (VT, \\v)" },
		{ 0x0C, "form feed (FF, \\f)" },
		{ 0x0D, "carriage return (CR, \\r)" },
		{ 0x0E, "shift out (SO)" },
		{ 0x0F, "shift in (SI)" },
		{ 0x10, "data link escape (DLE)" },
		{ 0x11, "device control 1 (DC1)" },
		{ 0x12, "device control 2 (DC2)" },
		{ 0x13, "device control 3 (DC3)" },
		{ 0x14, "device control 4 (DC4)" },
		{ 0x15, "negative acknowledge (NAK)" },
		{ 0x16, "synchronous idle (SYN)" },
		{ 0x17, "end of transmission block (ETB)" },
		{ 0x18, "cancel (CAN)" },
		{ 0x19, "end of medium (EM)" },
		{ 0x1A, "substitute (SUB)" },
		{ 0x1B, "escape (ESC, \\e)" },
		{ 0x1C, "file separator (FS)" },
		{ 0x1D, "group separator (GS)" },
		{ 0x1E, "record separator (RS)" },
		{ 0x1F, "unit separator (US)" },
		{ 0x20, "space" },
		{ 0x7F, "delete (DEL)" },
		/* TODO add C1 and other characters */
		{ 0x3000, "ideographic space" },
	};
	const size_t INVISIBLE_LENGTH = sizeof(INVISIBLE) / sizeof(*INVISIBLE);
	for (size_t i=0; i<INVISIBLE_LENGTH; i++) {
		if (codepoint == INVISIBLE[i].codepoint) {
			disp = INVISIBLE[i].description;
		}
	}
	if (!disp) {
		disp = (char *)buf;
	}
	printf(" " "%s" "\n", disp);
}

/*******************************************************************************
verb implementation functions
*******************************************************************************/

static void
_tableLookup(const struct pair * table, size_t table_length) {
	for (;;) {
		char buf[8];
		_readUtf8CharOrExit(buf, sizeof(buf));

		int found = 0;
		for (size_t i=0; i<table_length; i++) {
			const struct pair * pair = table + i;
			if (strncmp(buf, pair->key, pair_key_LENGTH) == 0) {
				printf("%s", pair->value);
				found = 1;
			}
		}
		if (!found) {
			printf("%s", buf);
		}
	}
}

static void
_decode(void) {
	for (;;) {
		/* UTF-8 encoding: https://en.wikipedia.org/wiki/UTF-8#Encoding */

		/* get UTF-8 character */
		uint8_t buf[8];
		size_t charSize = _readUtf8CharOrExit((char *)buf, sizeof(buf));

		/* convert UTF-8 character to codepoint */
		uint32_t codepoint = 0;
		if (charSize == 1) {
			/* is ASCII */
			codepoint = buf[0];
		}
		else if (charSize == 2) {
			codepoint =
				((buf[0] & 0b11111) << 6)
				|
				(buf[1] & 0b111111)
			;
		}
		else if (charSize == 3) {
			codepoint =
				((buf[0] & 0b1111) << 12)
				|
				((buf[1] & 0b111111) << 6)
				|
				(buf[2] & 0b111111)
			;
		}
		else if (charSize == 4) {
			codepoint =
				((buf[0] & 0b111) << 18)
				|
				((buf[1] & 0b111111) << 12)
				|
				((buf[2] & 0b111111) << 6)
				|
				(buf[3] & 0b111111)
			;
		}
		else {
			fprintf(stderr, "ERROR: _decode(): invalid char size (%zi)" "\n", charSize);
			exit(1);
		}

		_decode_printOrExit(buf, codepoint);
	}
}

static void
_randomcase(void) {
	/* seed random number generator */
	srand(time(NULL));

	for (;;) {
		/* get UTF-8 character */
		uint8_t buf[8];
		_readUtf8CharOrExit((char *)buf, sizeof(buf));
		uint8_t buf0 = buf[0];

		int doSwap = rand() % 2;
		int isUpperCase = ( (buf0 >= 'A') && (buf0 <= 'Z') );
		int isLowerCase = ( (buf0 >= 'a') && (buf0 <= 'z') );
		if ( !doSwap || ( !isUpperCase && !isLowerCase ) ) {
			/* print as-is */
			printf("%s", buf);
		}
		else {
			/* swap case */
			if (isUpperCase) {
				buf0 += ('a' - 'A');
			}
			else {
				buf0 -= ('a' - 'A');
			}
			printf("%c", buf0);
		}
	}
}

/*******************************************************************************
user interface
*******************************************************************************/

static void
_syntax(const char * self) {
	fprintf(stderr, "syntax: %s <verb>" "\n", self);
	fprintf(
		stderr,
		"verbs:" "\n"
			"\t" "aesthetic - latin characters and english punctuation into ａｅｓｔｈｅｔｉｃ ones" "\n"
			"\t" "decode - decode characters into unicode codepoints" "\n"
			"\t" "romanize - convert non-latin characters into latin ones" "\n"
			"\t" "randomcase - randomize case of latin characters" "\n"
	);
	fprintf(
		stderr,
		"examples:" "\n"
			"\t" "echo 'vaporwave' | %s aesthetic" "\n"
			"\t" "echo 'hello' | %s decode" "\n"
			"\t" "echo 'カタカナ' | %s romanize" "\n"
			"\t" "echo 'ひらがな' | %s romanize" "\n"
			"\t" "echo 'русский' | %s romanize" "\n"
		,
		self,
		self,
		self,
		self,
		self
	);
	exit(1);
}

int
main(int argc, char * argv[]) {
	const char * argv_SELF = argv[0];
	if (argc < 2) {
		_syntax(argv_SELF);
	}
	const char * argv_VERB = argv[1];

	if (
		(strcmp(argv_VERB, "a") == 0)
		||
		(strcmp(argv_VERB, "aesthetic") == 0)
	) {
		_tableLookup(table_AESTHETIC, table_AESTHETIC_LENGTH);
	}
	else if (
		(strcmp(argv_VERB, "d") == 0)
		||
		(strcmp(argv_VERB, "dec") == 0)
		||
		(strcmp(argv_VERB, "decode") == 0)
	) {
		_decode();
	}
	else if (
		(strcmp(argv_VERB, "r") == 0)
		||
		(strcmp(argv_VERB, "roman") == 0)
		||
		(strcmp(argv_VERB, "romanize") == 0)
	) {
		_tableLookup(table_ROMANIZE, table_ROMANIZE_LENGTH);
	}
	else if (
		(strcmp(argv_VERB, "rng") == 0)
		||
		(strcmp(argv_VERB, "rand") == 0)
		||
		(strcmp(argv_VERB, "random") == 0)
		||
		(strcmp(argv_VERB, "randomcase") == 0)
		||
		(strcmp(argv_VERB, "spongebob") == 0)
		||
		(strcmp(argv_VERB, "spongebobcase") == 0)
		||
		(strcmp(argv_VERB, "studly") == 0)
		||
		(strcmp(argv_VERB, "studlycaps") == 0)
	) {
		_randomcase();
	}
	else {
		_syntax(argv_SELF);
	}

	return 0;
}
