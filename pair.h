/*
licensed under CC0 (https://creativecommons.org/public-domain/cc0/)
2023 David DiPaola
*/

#ifndef __PAIR_H
#define __PAIR_H

#define pair_key_LENGTH 8
#define pair_value_LENGTH 8
struct pair {
	char key[pair_key_LENGTH];
	char value[pair_value_LENGTH];
};

#endif
